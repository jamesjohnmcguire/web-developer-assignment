@extends('layouts.app')

@section('title', 'Welcome')

@section('content')
    <h1>Books</h1>
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif

    <form name="search" action="search" class="search-form">
      <input type="text" id="query" name="query" class="" placeholder=""  value="{{ old('query') }}" autocomplete="query" autofocus size="40">
      <button type="submit" class="">Search</button>
    </form>

    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th scope="col">@sortablelink('author')</th>
          <th scope="col">@sortablelink('title')</th>
          <th scope="col" class="link-column"></th>
          <th scope="col" class="link-column"></th>
        </tr>
      </thead>
      <tbody>
        @if ($books->count() == 0)
        <tr>
          <td colspan="4">No books to display.</td>
        </tr>
        @endif

        @foreach ($books as $book)
        <tr>
          <td>{{$book->author}}</td>
          <td>{{$book->title}}</td>
          <td><a href="/edit/{{$book->id}}">Edit<a></td>
          <td><a href="/delete/{{$book->id}}">Delete<a></td>
        </tr>
        @endforeach

      </tbody>
    </table>
@endsection
