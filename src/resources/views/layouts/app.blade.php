<!DOCTYPE html>
<html>
  <head>
    <title>@yield('pageTitle')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Tester">
    <meta name="description" content="">

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/assignment.css') }}">

    @yield('additional_css')
  </head>
  <body>
    <div class="container-fluid">
      <div class="row main-section">
        <div class="col-sm-3">
            @section('sidebar')
            <nav class="">
              <div class="navbar navbar-expand-lg">
                <div class="container">
                  <ul class="left">
                  <li class="nav-item main-menu-item"><a class="nav-link" href="/">Home</a></li>
                  <li class="nav-item main-menu-item"><a class="nav-link" href="/add">Add a Book</a></li>
                  <li class="nav-item main-menu-item"><a class="nav-link" href="/export">Export</a></li>
                  </ul>
                </div>
              </div>
            </nav>
            @show
        </div>
        <div class="col main-right-section">
          @yield('content')
        </div>
      </div>
    </div>

    <script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>

    @yield('additional_javascript')

  </body>
</html>
