@extends('layouts.app')

@section('pageTitle')
    {{ $pageTitle }}
@endsection

@section('content')
    <h1>{{ $pageTitle }}</h1>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form name="export" action="exporting">

      <label class="form-check-inline">Export Type:</label>
      <div class="form-check-inline">
        <input type="radio" name="type" id="csv" value="csv" @if(old('type') != 'xml') checked @endif>
        <label class="form-check-label" for="csv">CSV</label>
      </div>
      <div class="form-check-inline">
        <input type="radio" name="type" id="xml" value="xml" @if(old('type') == 'xml') checked @endif>
        <label class="form-check-label" for="xml">XML</label>
      </div>

      <div class="form-section">
        <input type="checkbox" name="title" value="{{ old('title') }}" >
        <label>Include Title</label>
      </div>
      <div class="form-section">
        <input type="checkbox" name="author" value="{{ old('author') }}" >
        <label>Include Author</label>
      </div>

      <div class="form-section">
        <button type="submit" class="">Export</button>
      </div>

    </form>
@endsection
