@extends('layouts.app')

@section('pageTitle')
    {{ $pageTitle }}
@endsection

@section('content')
    <h1>{{ $pageTitle }}</h1>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form name="add" method="post" action="{{route($route, [$book->id])}}">
      {{ csrf_field() }}
      {{ method_field($method) }}
      <div class="">Title:</div>
      <input type="text" id="title" name="title" class="" placeholder=""  value="{{ old('title', $book->title) }}" autocomplete="title" autofocus size="40">
      <div class="">Author:</div>
      <input type="text" id="author" name="author" class="" placeholder=""  value="{{ old('author', $book->author) }}" autocomplete="author" autofocus size="40">
      <button type="submit" class="">{{ $buttonTitle }}</button>
    </form>
@endsection
