@extends('layouts.app')

@section('pageTitle')
    {{ $pageTitle }}
@endsection

@section('content')
    <h1>{{ $pageTitle }}</h1>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form name="delete" method="POST" action="{{route($route, [$book->id])}}">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
      <div class="">Title: {{ $book->title }}</div>
      <div class="">Author: {{ $book->author }}</div>
      <button type="submit" class="">{{ $buttonTitle }}</button>
    </form>
@endsection
