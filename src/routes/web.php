<?php

use App\Http\Controllers\BookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BookController::class, 'index']);

Route::get('/add', [BookController::class, 'create']);
Route::get('/edit/{id}', [BookController::class, 'edit']);
Route::get('/delete/{id}', [BookController::class, 'delete']);
Route::get('/search', [BookController::class, 'search']);
Route::get('/export', [BookController::class, 'export']);
Route::get('/exporting', [BookController::class, 'exporting']);

Route::delete('/destroy/{id}', [BookController::class, 'destroy'])->name('destroy');

Route::post('/store', [BookController::class, 'store'])->name('store');

Route::put('/update/{id}', [BookController::class, 'update'])->name('update');
