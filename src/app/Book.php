<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Scout\Searchable;

class Book extends Model
{
    use Searchable;
    use Sortable;

    public $sortable =
    [
        'title',
        'author'
    ];
}
