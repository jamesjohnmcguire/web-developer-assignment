<?php

namespace App\Http\Controllers;

use App\Book;
use App\Exports\BooksExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BookController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $book = new Book;

        return view('edit', ['route' => 'store', 'method' => 'POST', 'pageTitle' => 'Add a Book', 'buttonTitle' => 'Add'])->with('book', $book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $book = Book::find($id);

        return view('delete', ['route' => 'destroy', 'pageTitle' => 'Delete Book', 'buttonTitle' => 'Delete'])->with('book', $book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::destroy($id);

        return redirect('/')->with('message', 'Book Deleted');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        return view('edit', ['route' => 'update', 'method' => 'PUT', 'pageTitle' => 'Edit Book', 'buttonTitle' => 'Update'])->with('book', $book);
    }

    /**
     * Export listings.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        return view('export', ['route' => 'exporting', 'pageTitle' => 'Export Book List', 'buttonTitle' => 'Export']);
    }

    /**
     * The actual exporting of listings.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exporting(Request $request)
    {
        $result = null;
        $exportType = 'csv';
        $includeTitle = false;
        $includeAuthor = false;

        $request->validate([
            'type' => 'in:csv,xml',
            'title' => 'required_unless:author,""',
            'author' => 'required_unless:title,""'
        ]);

        $exportType = $request->input('type');

        if($request->has('title'))
        {
            $includeTitle = true;
        }

        if($request->has('author'))
        {
            $includeAuthor = true;
        }

        $books = Book::sortable()->get();

        if ($exportType == 'xml')
        {
            self::saveToXml($books, $includeTitle, $includeAuthor);
        }
        else
        {
            $booksExport = self::saveToCsv($includeTitle, $includeAuthor);
            $result = Excel::download($booksExport, 'books.csv');
        }

        if (!headers_sent())
        {
            redirect('/')->with('message', 'Book List Exported');
        }

        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::sortable()->get();

        return view('home', ['title' => 'Welcome'])->with('books', $books);
    }

    /**
     * Display a listing of the resource based upon search query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $query = $request->input('query');

        $books = Book::query()
            ->where('title', 'LIKE', "%{$query}%")
            ->orWhere('author', 'LIKE', "%{$query}%")
            ->get();

        return view('home', ['pageTitle' => 'Search Results'])->with('books', $books);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =
        [
            'title' => 'required',
            'author' => 'required',
        ];

        $validated = $request->validate($rules);

        $book = new Book;

        $book->title = $request->title;
        $book->author = $request->author;

        $book->save();

        return redirect('/')->with('message', 'Book Added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =
        [
            'title' => 'required',
            'author' => 'required',
        ];

        $validated = $request->validate($rules);

        $book = Book::find($id);

        $book->title = $request->title;
        $book->author = $request->author;

        $book->update();

        return redirect('/')->with('message', 'Book Updated');
    }

    private static function saveToCsv($includeTitle, $includeAuthor)
    {
        $booksExport = new BooksExport;

        $columns = [];
        if ($includeTitle == true)
        {
            $columns[] = 'title';
        }

        if ($includeAuthor == true)
        {
            $columns[] = 'author';
        }

        $booksExport->setFields($columns);

        return $booksExport;
    }

    private static function saveToXml($books, $includeTitle, $includeAuthor)
    {
        $xml = new \XMLWriter();

        $xml->openURI('php://output');
        $xml->startDocument('1.0');
        $xml->startElement('books');
    
        foreach ($books as $book)
        {
            $xml->startElement('book');

            if ($includeTitle == true)
            {
                $xml->writeElement('Title', $book->title);
            }

            if ($includeAuthor == true)
            {
                $xml->writeElement('Author', $book->author);
            }

            $xml->endElement();
        }
    
        $xml->endElement();

        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="text.xml"');
        $xml->endDocument();
        $xml->flush();
   }
}
