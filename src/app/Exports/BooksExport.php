<?php

namespace App\Exports;

use App\Book;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BooksExport implements FromCollection, WithMapping
{
    public $columnNames;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Book::all();
    }

    public function map($row): array
    {
        $fields = [];

        foreach ($this->columnNames as $column)
        {
            $fields[] = $row->$column;
        }

        return $fields;
    }

    public function setFields($columnNames)
    {
        $this->columnNames = $columnNames;
    }
}
